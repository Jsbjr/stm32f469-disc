#![no_main]
#![no_std]

use core::cell::RefCell;
use core::ops::DerefMut;
use cortex_m_rt::entry;
use panic_halt as _;

use cortex_m::interrupt::{free, Mutex};

use stm32f469_disc::{
    buttons::{self, Buttons},
    hal::gpio::Edge,
    hal::{interrupt, prelude::*, stm32},
    led::{Color, Leds},
};

static BUTTON: Mutex<RefCell<Option<Buttons>>> = Mutex::new(RefCell::new(None));
static LED: Mutex<RefCell<Option<Leds>>> = Mutex::new(RefCell::new(None));

#[entry]
fn main() -> ! {
    if let (Some(mut p), Some(_cp)) = (stm32::Peripherals::take(), cortex_m::Peripherals::take()) {
        let mut syscfg = p.SYSCFG.constrain();

        let gpiod = p.GPIOD.split();
        let gpiog = p.GPIOG.split();
        let gpiok = p.GPIOK.split();

        // Initialize on-board LEDs
        let mut leds = Leds::new(gpiod, gpiog, gpiok);
        leds[Color::Green].off();
        leds[Color::Orange].off();
        leds[Color::Red].off();
        leds[Color::Blue].off();

        let gpioa = p.GPIOA.split();

        let mut buttons = Buttons::new(gpioa);

        buttons[buttons::Color::Blue].make_interrupt_source(&mut syscfg);
        buttons[buttons::Color::Blue].trigger_on_edge(&mut p.EXTI, Edge::RISING);
        buttons[buttons::Color::Blue].enable_interrupt(&mut p.EXTI);

        free(|cs| {
            BUTTON.borrow(cs).replace(Some(buttons));
            LED.borrow(cs).replace(Some(leds));
        });

        //enable interrupts
        stm32::NVIC::unpend(stm32::Interrupt::EXTI0);
        unsafe {
            stm32::NVIC::unmask(stm32::Interrupt::EXTI0);
        }
        loop {}
    } else {
        loop {}
    }
}

#[interrupt]
fn EXTI0() {
    free(|cs| {
        let (mut buttons_ref, mut leds_ref) =
            (BUTTON.borrow(cs).borrow_mut(), LED.borrow(cs).borrow_mut());
        if let (Some(ref mut buttons), Some(ref mut leds)) =
            (buttons_ref.deref_mut(), leds_ref.deref_mut())
        {
            if buttons[buttons::Color::Blue].check_interrupt() {
                buttons[buttons::Color::Blue].clear_interrupt_pending_bit();
                leds[Color::Green].toogle();
                leds[Color::Orange].toogle();
                leds[Color::Red].toogle();
                leds[Color::Blue].toogle();
            }
        }
    });
}
