#![no_main]
#![no_std]

use panic_halt as _;
use stm32f469_disc::hal::{
    prelude::*,
    serial::{config::Config, Serial},
    stm32,
};

use core::fmt::Write;
use cortex_m::Peripherals;
use cortex_m_rt::entry;

#[entry]
fn main() -> ! {
    if let (Some(p), Some(_cp)) = (stm32::Peripherals::take(), Peripherals::take()) {
        let rcc = p.RCC.constrain();

        // Set some clocks. This works but I don't know what is useful there.
        let clocks = rcc
            .cfgr
            .use_hse(8.mhz())
            .sysclk(168.mhz())
            .pclk1(24.mhz())
            .i2s_clk(86.mhz())
            .require_pll48clk()
            .freeze();

        let gpiob = p.GPIOB.split();
        let tx = gpiob.pb10.into_alternate_af7();
        let rx = gpiob.pb11.into_alternate_af7();

        let cfg = Config::default().baudrate(115_200.bps());
        let usart3 = p.USART3.into();

        if let Ok(mut serial) = Serial::new(usart3, (tx, rx), cfg, clocks) {
            loop {
                while !serial.is_rxne() {}
                if let Ok(word) = serial.read() {
                    write!(serial, "{}", word as char).unwrap();
                }
            }
        } else {
            loop {}
        }
    } else {
        loop {}
    }
}
