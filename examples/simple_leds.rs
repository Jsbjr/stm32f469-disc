#![no_main]
#![no_std]

use panic_halt as _;

use stm32f469_disc::{
    hal::stm32,
    hal::{delay::Delay, prelude::*},
    led::{Color, Leds},
};

use cortex_m::peripheral::Peripherals;

use cortex_m_rt::entry;

#[entry]
fn main() -> ! {
    if let (Some(p), Some(cp)) = (stm32::Peripherals::take(), Peripherals::take()) {
        let gpiod = p.GPIOD.split();
        let gpiog = p.GPIOG.split();
        let gpiok = p.GPIOK.split();

        // Initialize on-board LEDs
        let mut leds = Leds::new(gpiod, gpiog, gpiok);

        // Constrain clock registers
        let rcc = p.RCC.constrain();

        // Configure clock to 168 MHz (i.e. the maximum) and freeze it
        let clocks = rcc.cfgr.sysclk(168.mhz()).freeze();

        // Get delay provider
        let mut delay = Delay::new(cp.SYST, clocks);

        loop {
            // Turn LEDs on one after the other with 500ms delay between them
            leds[0].on();
            delay.delay_ms(500_u16);
            leds[Color::Orange].on();
            delay.delay_ms(500_u16);
            leds[2].on();
            delay.delay_ms(500_u16);
            leds[Color::Blue].on();
            delay.delay_ms(500_u16);

            // Turn LEDs off one after the other with 500ms delay between them
            leds[Color::Green].off();
            delay.delay_ms(500_u16);
            leds[Color::Orange].off();
            delay.delay_ms(500_u16);
            leds[Color::Red].off();
            delay.delay_ms(500_u16);
            leds[Color::Blue].off();
            delay.delay_ms(500_u16);
        }
    } else {
        loop {}
    }
}
