#![deny(unsafe_code)]
#![no_main]
#![no_std]

use core::cell::RefCell;
use core::ops::DerefMut;
use cortex_m::interrupt::{free, Mutex};
use cortex_m::peripheral::syst::SystClkSource;
use cortex_m_rt::{entry, exception};
use panic_halt as _;

use stm32f469_disc::{
    hal::{prelude::*, stm32},
    led::{Color, Leds},
};

static LED: Mutex<RefCell<Option<Leds>>> = Mutex::new(RefCell::new(None));

#[entry]
fn main() -> ! {
    if let (Some(p), Some(cp)) = (stm32::Peripherals::take(), cortex_m::Peripherals::take()) {
        let gpiod = p.GPIOD.split();
        let gpiog = p.GPIOG.split();
        let gpiok = p.GPIOK.split();
        let mut leds = Leds::new(gpiod, gpiog, gpiok);

        leds[Color::Green].on();
        leds[Color::Red].on();
        leds[Color::Orange].off();
        leds[Color::Blue].off();

        free(|cs| {
            LED.borrow(cs).replace(Some(leds));
        });

        let mut syst = cp.SYST;

        // configures the system timer to trigger a SysTick exception every second
        syst.set_clock_source(SystClkSource::Core);
        // this is configured for the LM3S6965 which has a default CPU clock of 12 MHz
        syst.set_reload(28_000_000);
        syst.clear_current();
        syst.enable_counter();
        syst.enable_interrupt();

        loop {}
    } else {
        loop {}
    }
}
#[exception]
fn SysTick() {
    free(|cs| {
        let mut leds_ref = LED.borrow(cs).borrow_mut();
        if let Some(ref mut leds) = leds_ref.deref_mut() {
            leds[Color::Green].toogle();
            leds[Color::Red].toogle();
            leds[Color::Orange].toogle();
            leds[Color::Blue].toogle();
        }
    });
}
