set history save on
set history size 10000
set history filename .gdb_history
target extended-remote :3333

# print demangled symbols
set print demangle on

set backtrace limit 32

# detect unhandled exceptions, hard faults and panics
break DefaultHandler
break HardFault
break rust_begin_unwind

# *try* to stop at the user entry point (it might be gone due to inlining)
break main

# # send captured ITM to the file itm.fifo
# # (the microcontroller SWO pin must be connected to the programmer SWO pin)
# # 168000000 must match the core clock frequency
monitor tpiu config internal itm.txt uart off 168000000

# # OR: make the microcontroller SWO pin output compatible with UART (8N1)
# # 2000000 is the frequency of the SWO pin
# monitor tpiu config external uart off 18000000 2000000

# # enable ITM port 1
monitor itm port 1 on

load
continue
