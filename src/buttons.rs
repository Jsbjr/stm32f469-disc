use hal::gpio::{
    gpioa::{self, PA, PA0},
    Edge, ExtiPin, Input, PullDown,
};
use hal::stm32::EXTI;
use hal::syscfg::SysCfg;

pub type BTN2 = PA0<Input<PullDown>>;

pub enum Color {
    Blue,
}
pub struct Buttons {
    buttons: [Button; 1],
}

impl core::ops::Deref for Buttons {
    type Target = [Button];

    fn deref(&self) -> &[Button] {
        &self.buttons
    }
}
impl core::ops::DerefMut for Buttons {
    fn deref_mut(&mut self) -> &mut [Button] {
        &mut self.buttons
    }
}
impl core::ops::Index<usize> for Buttons {
    type Output = Button;

    fn index(&self, i: usize) -> &Button {
        &self.buttons[i]
    }
}
impl core::ops::Index<Color> for Buttons {
    type Output = Button;

    fn index(&self, c: Color) -> &Button {
        &self.buttons[c as usize]
    }
}

impl core::ops::IndexMut<usize> for Buttons {
    fn index_mut(&mut self, i: usize) -> &mut Button {
        &mut self.buttons[i]
    }
}
impl core::ops::IndexMut<Color> for Buttons {
    fn index_mut(&mut self, c: Color) -> &mut Button {
        &mut self.buttons[c as usize]
    }
}

pub struct Button {
    pin: PA<Input<PullDown>>,
}

impl Buttons {
    pub fn new(gpioa: gpioa::Parts) -> Self {
        let b2 = gpioa.pa0.into_pull_down_input();
        Buttons {
            buttons: [b2.into()],
        }
    }
}

impl Into<Button> for BTN2 {
    fn into(self) -> Button {
        Button {
            pin: self.downgrade(),
        }
    }
}

impl Button {
    pub fn make_interrupt_source(&mut self, syscfg: &mut SysCfg) {
        self.pin.make_interrupt_source(syscfg);
    }
    pub fn trigger_on_edge(&mut self, exti: &mut EXTI, level: Edge) {
        self.pin.trigger_on_edge(exti, level);
    }
    pub fn enable_interrupt(&mut self, exti: &mut EXTI) {
        self.pin.enable_interrupt(exti);
    }
    pub fn disable_interrupt(&mut self, exti: &mut EXTI) {
        self.pin.disable_interrupt(exti);
    }
    pub fn clear_interrupt_pending_bit(&mut self) {
        self.pin.clear_interrupt_pending_bit();
    }
    pub fn check_interrupt(&self) -> bool {
        self.pin.check_interrupt()
    }
}
