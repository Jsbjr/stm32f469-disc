use hal::prelude::*;

use hal::gpio::gpiod::{self, PD, PD4, PD5};
use hal::gpio::gpiog::{self, PG, PG6};
use hal::gpio::gpiok::{self, PK, PK3};
use hal::gpio::{Output, PushPull};

pub type LD1 = PG6<Output<PushPull>>;
pub type LD2 = PD4<Output<PushPull>>;
pub type LD3 = PD5<Output<PushPull>>;
pub type LD4 = PK3<Output<PushPull>>;

pub enum Color {
    Green,
    Orange,
    Red,
    Blue,
}

enum PinType {
    PD(PD<Output<PushPull>>),
    PG(PG<Output<PushPull>>),
    PK(PK<Output<PushPull>>),
}

pub struct Leds {
    leds: [Led; 4],
}
impl Leds {
    pub fn new(gpiod: gpiod::Parts, gpiog: gpiog::Parts, gpiok: gpiok::Parts) -> Self {
        let green = gpiog.pg6.into_push_pull_output();
        let orange = gpiod.pd4.into_push_pull_output();
        let red = gpiod.pd5.into_push_pull_output();
        let blue = gpiok.pk3.into_push_pull_output();
        Leds {
            leds: [green.into(), orange.into(), red.into(), blue.into()],
        }
    }
}

impl core::ops::Deref for Leds {
    type Target = [Led];

    fn deref(&self) -> &[Led] {
        &self.leds
    }
}
impl core::ops::DerefMut for Leds {
    fn deref_mut(&mut self) -> &mut [Led] {
        &mut self.leds
    }
}
impl core::ops::Index<usize> for Leds {
    type Output = Led;

    fn index(&self, i: usize) -> &Led {
        &self.leds[i]
    }
}
impl core::ops::Index<Color> for Leds {
    type Output = Led;

    fn index(&self, c: Color) -> &Led {
        &self.leds[c as usize]
    }
}

impl core::ops::IndexMut<usize> for Leds {
    fn index_mut(&mut self, i: usize) -> &mut Led {
        &mut self.leds[i]
    }
}
impl core::ops::IndexMut<Color> for Leds {
    fn index_mut(&mut self, c: Color) -> &mut Led {
        &mut self.leds[c as usize]
    }
}

pub struct Led {
    pin: PinType,
}

impl Into<Led> for LD1 {
    fn into(self) -> Led {
        Led {
            pin: PinType::PG(self.downgrade()),
        }
    }
}
impl Into<Led> for LD2 {
    fn into(self) -> Led {
        Led {
            pin: PinType::PD(self.downgrade()),
        }
    }
}
impl Into<Led> for LD3 {
    fn into(self) -> Led {
        Led {
            pin: PinType::PD(self.downgrade()),
        }
    }
}
impl Into<Led> for LD4 {
    fn into(self) -> Led {
        Led {
            pin: PinType::PK(self.downgrade()),
        }
    }
}

impl Led {
    pub fn off(&mut self) {
        match &mut self.pin {
            PinType::PD(pin) => pin.set_high().ok(),
            PinType::PG(pin) => pin.set_high().ok(),
            PinType::PK(pin) => pin.set_high().ok(),
        };
    }

    pub fn on(&mut self) {
        match &mut self.pin {
            PinType::PD(pin) => pin.set_low().ok(),
            PinType::PG(pin) => pin.set_low().ok(),
            PinType::PK(pin) => pin.set_low().ok(),
        };
    }

    pub fn toogle(&mut self) {
        match &mut self.pin {
            PinType::PD(pin) => {
                if let Ok(true) = pin.is_low() {
                    pin.set_high().ok()
                } else {
                    pin.set_low().ok()
                }
            }
            PinType::PG(pin) => {
                if let Ok(true) = pin.is_low() {
                    pin.set_high().ok()
                } else {
                    pin.set_low().ok()
                }
            }
            PinType::PK(pin) => {
                if let Ok(true) = pin.is_low() {
                    pin.set_high().ok()
                } else {
                    pin.set_low().ok()
                }
            }
        };
    }
}
